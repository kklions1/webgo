/*********************************************************************
 * Name: Ian Pirie
 * Date: 12/12/2018
 * File: index.h
 * 
 * Description: The header file for the index of all files in the 
 * project currently.
 * ******************************************************************/

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
class index
{
    public:
        /**
         * Function: Default Constructor
         * Return Type: N/a
         * Parameters: N/a
         * 
         * Description: The idea behind this is, you start a project,
         * it creates the base for a website main.html
         * */
        index();

        /**
         * Function: Constructor1
         * Return Type: N/a
         * Parameters: std::string filename
         * 
         * Description: This will take an index file in and open up 
         * everything in the index, including other files.
         * */
        index(std::string filename);

        /**
         * Function: insertIndex
         * Return Type: Void
         * Parameters: std::string str
         * 
         * Description: This will check to see if there is a file 
         * with the same file name and if none are found, insert a new 
         * file to the setup.
         * */
        void insertIndex(std::string str);

        /**
         * Function: insertIndex
         * Return Type: void
         * Parameters: std::string str, std::ostream os
         * 
         * Description: Same as the other one but made so that you can
         * include it in various other formats, like GUI eventually.
         * */
        void insertIndex(std::string str, std::ostream& os);

        /**
         * Function: removeIndex
         * Return Type: void
         * Parameters: std:string str
         * 
         * Desription: Searches through the index and deletes that file 
         * from the index if found.
         * */
        void removeIndex(std::string str);
    private:

        /// This holds all the files included in the project.
        std::vector<std::string> files;
};