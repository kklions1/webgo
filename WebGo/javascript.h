/*********************************************************************
 * Name: Ian Pirie
 * Date: 12/12/2018
 * File: javascript.h
 * 
 * Description: This is the class that looks at the various adding 
 * situations. EX:
 * 
 * You create a radio button with HTML, this will have the function
 * in it that autogenerates the javascript function and the control
 * structure inside of it to help figure out which option was 
 * selected. 
 * ******************************************************************/